This repository is a programming challenge by Oxalis Games.
To get started please **Fork** the repository into your own BitBucket account before making any commits.
When you have finished your solution please give us read access to your repository and send the link.

It contains a Unity project (2017.2.0p4) written in C#.

Open the Main Menu scene and run it in the IDE.
You should see a scroll view with 1,000 items in that can be scrolled vertically.
Each item contains an image button and a text object.
Each image is unique on purpose to create a performance lag.

There is a frame rate UI component in the Main Menu scene, this is just a guide.

We would like you to optimize the code to be as efficient as possible.
Maintain existing functionality.
The grid should fill the any screen.

Things to consider:
- maybe you don't need to instantiate 1,000 prefabs
- maybe you don't need all those Rect Transforms
- minimise function calls
- only update things when necessary

We are looking for an optimal solution and would like you to make the MainMenu scene run as smooth as possible.

(Do not bin the Scroll View)

Feel free to ask us questions.